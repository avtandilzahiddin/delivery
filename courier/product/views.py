from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet

from product.models import Product, Categories
from product.permissions import Categories_IsStuffOrReadOnly, Product_IsStuffOrReadOnly
from product.serializers import ProductSerializer, CategoriesSerializer


class CategoriesView(ModelViewSet):
    serializer_class = CategoriesSerializer
    queryset = Categories.objects.all()
    lookup_field = 'pk'
    permission_classes = (Categories_IsStuffOrReadOnly, )


class ProductView(ModelViewSet):
    serializer_class = ProductSerializer
    queryset = Product.objects.all()
    lookup_field = 'pk'
    permission_classes = (Product_IsStuffOrReadOnly, )