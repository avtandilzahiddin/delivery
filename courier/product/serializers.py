from rest_framework import serializers

from product.models import Categories, Product


class CategoriesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Categories
        fields = ('id', 'name', 'description')

class ProductSerializer(serializers.ModelSerializer):
    category = CategoriesSerializer

    class Meta:
        model = Product
        fields = ('id', 'name', 'price', 'description', 'image', 'category')

