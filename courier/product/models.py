from django.db import models

class Categories(models.Model):
    name = models.CharField('Название категории', max_length=255)
    description = models.TextField('Описание')

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'
        ordering = ('id',)

    def __str__(self):
        return f'{self.name} | {self.description}'

class Product(models.Model):
    name = models.CharField('Товар', max_length=255, blank=True, null=True)
    price = models.PositiveIntegerField('Цена', default=0)
    description = models.TextField('Описание', null=True)
    image = models.FileField('Фото', upload_to='product_images/', null=True)
    category = models.ForeignKey('product.Categories', models.CASCADE, related_name='category')

    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'
        ordering = ('id',)

    def __str__(self):
        return f'{self.name} | {self.category.name}'


