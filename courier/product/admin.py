from django.contrib import admin

from product.models import Product, Categories

admin.site.register(Product)

admin.site.register(Categories)