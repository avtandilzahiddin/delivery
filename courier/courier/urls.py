"""courier URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from product.views import ProductView, CategoriesView
from user.views import ProfileRegisterView, ProfileLoginView, StuffRegisterView, StuffLoginView, CourierRegisterView, \
    CourierLoginView

urlpatterns = [
    path('admin/', admin.site.urls),
    #-------------------------------------------------------
    path('profile/register', ProfileRegisterView.as_view()),
    path('profile/login', ProfileLoginView.as_view()),
    #-------------------------------------------------------
    path('stuff/register', StuffRegisterView.as_view()),
    path('stuff/login', StuffLoginView.as_view()),
    #-------------------------------------------------------
    path('courier/register', CourierRegisterView.as_view()),
    path('courier/login', CourierLoginView.as_view()),
    #-------------------------------------------------------
    path('categories/', CategoriesView.as_view({'get': 'list', 'post': 'create'})),
    path('category/<int:pk>', CategoriesView.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'})),
    #-------------------------------------------------------
    path('products/', ProductView.as_view({'get': 'list', 'post': 'create'})),
    path('product/<int:pk>', ProductView.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'})),

    path('order/', include('order.urls')),

]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
