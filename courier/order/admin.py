from django.contrib import admin

from order.models import Order, OrderProducts


class OrderProductsInline(admin.TabularInline):
    model = OrderProducts
    extra = 0


class OrderAdmin(admin.ModelAdmin):
    inlines = [OrderProductsInline]


admin.site.register(Order, OrderAdmin)
