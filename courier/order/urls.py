from django.urls import path

from order.views import OrderView, OrderViewForStaff

# OrderViewNoSer

urlpatterns = [
    path('', OrderView.as_view({'post': 'create', 'get': 'list'})),
    path('<int:pk>', OrderView.as_view({'get': 'retrieve', 'delete': 'destroy', 'post': 'update'})),
    path('staff/', OrderViewForStaff.as_view({'post': 'create'}))
    # path('no_ser', OrderViewNoSer.as_view({'post': 'create'})),
]

