from rest_framework import serializers

from order.models import Order, OrderProducts
from product.models import Product
from user.models import Profiles


class OrderProductsSerializer(serializers.ModelSerializer):

    class Meta:
        model = OrderProducts
        fields = ('product', 'count')


class OrderSerializer(serializers.ModelSerializer):
    order_products = OrderProductsSerializer(many=True, read_only=True)

    class Meta:
        model = Order
        fields = ('id', 'client', 'address', 'order_products', 'total_price', 'date')

    def create(self, validated_data):
        user = self.context.get('request').user.profile
        address = validated_data.get('address')
        order_products = validated_data.get('order_products')

        order = Order.objects.create(client=user, address=address)

        total_sum = 0

        for product in order_products:
            price = Product.objects.get(id=product.get('product').id).price
            total_sum += price * product.get('count')

            OrderProducts.objects.create(
                order=order,
                product=product.get('product'),
                count=product.get('count')
            )
        order.total_price = total_sum
        order.save()

        return order