from rest_framework.permissions import BasePermission, SAFE_METHODS


class ClientOrReadOnly(BasePermission):
    def has_permission(self, request, view):
        if request.method == 'GET':
            return True
        return not request.user.is_staff