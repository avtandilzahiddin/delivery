from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from order.models import Order, OrderProducts
from order.permissions import ClientOrReadOnly
from order.serializers import OrderSerializer
from product.models import Product


class OrderViewForStaff(ModelViewSet):

    def create(self, request, *args, **kwargs):
        address = request.data.get('address')
        order_products = request.data.get('order_products')

        order = Order.objects.create(address=address)

        total_sum = 0

        for product in order_products:
            price = Product.objects.get(id=product.get('product')).price
            total_sum += price * product.get('count')

            OrderProducts.objects.create(
                order=order,
                product_id=product.get('product'),
                count=product.get('count')
            )

        order.total_price = total_sum
        order.save()

        return Response('Success')


class OrderView(ModelViewSet):
    serializer_class = OrderSerializer
    queryset = Order.objects.all()
    lookup_field = 'pk'
    permission_classes = (ClientOrReadOnly, )

